.. _how-to-dump-registers:

=====================
How to Dump Registers
=====================

.. note::

   This document is a placeholder for updated content. The linked page may be
   outdated.

See `How to Dump Registers`_.

.. _How to Dump Registers: https://www.intel.com/content/www/us/en/docs/graphics-for-linux/developer-reference/1-0/reg-dumper.html
