Intel Graphics for Linux
========================

Developer documentation for the **drm/i915** and **drm/xe** kernel drivers,
including bug filing and debugging instructions.

To contribute to the documentation, please see the `documentation GitLab project
page`_, in particular `documentation issues`_ and `merge requests`_.

This documentation is specifically about the upstream kernel drivers. See
dedicated sites for `Programmer's Reference Manuals`_ and `Product
Specifications (ARK)`_.

.. _documentation GitLab project page: https://gitlab.freedesktop.org/drm/intel-docs

.. _documentation issues: https://gitlab.freedesktop.org/drm/intel-docs/-/issues

.. _merge requests: https://gitlab.freedesktop.org/drm/intel-docs/-/merge_requests

.. _Programmer's Reference Manuals: https://www.intel.com/content/www/us/en/docs/graphics-for-linux/developer-reference/1-0/overview.html

.. _Product Specifications (ARK): https://ark.intel.com/content/www/us/en/ark.html#@Processors

.. toctree::
   :maxdepth: 2

   Introduction <self>
   how-to-file-i915-bugs
   how-to-build-the-kernel
   how-to-get-the-gpu-error-state
   how-to-debug-suspend-resume-issues
   how-to-dump-registers
   vbt
   debugfs
