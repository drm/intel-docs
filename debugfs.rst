.. _debugfs:

=======
Debugfs
=======

The debugfs is a special filesystem that contains kernel and driver debug
information and options. The debugfs is typically mounted at
``/sys/kernel/debug``, and accessible only to the root user by default. Please
refer to your distro documentation for details.

The debugfs for i915, and GPU drivers in general, is located at
``/sys/kernel/debug/dri/<N>/`` where ``<N>`` is the minor device number, usually
``0`` for integrated GPU and i915. If you have more than one GPU, they'll each
have their own numbers and directories.

.. note::

   This documentation usually refers to ``/sys/kernel/debug/dri/0/`` but the
   ``0`` should be replaced with the appropriate number for your device.

There are subdirectories for each crtc (named ``crtc-<index>``) and connector
(named ``<connector type>-<type id>``, for example ``DP-1`` and ``HDMI-A-1``).
