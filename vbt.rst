.. _vbt:

=======================
Video BIOS Tables (VBT)
=======================

With the i915 driver probed, the VBT is available in :ref:`debugfs`.

.. code-block:: bash

   /sys/kernel/debug/dri/0/i915_vbt

The VBT contents may be decoded using the IGT_ tool `intel_vbt_decode(1)`_, but
please always attach the binary file, not the decoded output to bugs.

The contents of the VBT are independent of the driver or kernel version. It
*may* be different between UEFI and legacy boot; please prefer UEFI boot.

Alternative ways to get the VBT
===============================

ACPI OpRegion
-------------

For some systems, the VBT is also part of the ACPI OpRegion, available in
:ref:`debugfs`.

.. code-block:: bash

   /sys/kernel/debug/dri/0/i915_opregion

Usually, where this works, the ``i915_vbt`` file also works.

intel_bios_dumper
-----------------

`intel_bios_dumper(1)`_ is an IGT_ tool to save the entire Video BIOS to a file.

PCI ROM
-------

You may also be able to dump the Video BIOS via the PCI ROM.

.. code-block:: bash

   echo 1 > /sys/devices/pci0000:00/0000:00:02.0/rom
   cat /sys/devices/pci0000:00/0000:00:02.0/rom > vbios.dump
   echo 0 > /sys/devices/pci0000:00/0000:00:02.0/rom

Adjust the PCI device numbers as needed for your device.

.. _IGT: https://gitlab.freedesktop.org/drm/igt-gpu-tools

.. _intel_vbt_decode(1): https://manpages.debian.org/unstable/intel-gpu-tools/intel_vbt_decode.1.en.html

.. _intel_bios_dumper(1): https://manpages.debian.org/unstable/intel-gpu-tools/intel_bios_dumper.1.en.html
